// script utilizzato per generare gli stili degli elementi grafici
// che si vogliono disporre a cerchio (con trasformazioni CSS3)

var NUM_ELEM = 30;

for(var i=0; i<NUM_ELEM; i++) {
  var val = i*(360/NUM_ELEM);
  var s = '.deg' + val + ' { transform: ';
  if (val)
  	s += 'rotate(' + val + 'deg) ';
  s += 'translate(12em)'
  if (val)
		s += ' rotate(-' + val + 'deg)' 
  s += '; }'
  
  console.log(s);
}
