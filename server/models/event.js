'use strict';

module.exports = function(Event) {

  Event.afterRemote('create', function(ctx, event, next) {
    console.log(event);
    if (event.name == 'config') {
      Event.app.io.sockets.emit('config', event.data);
    }
    next();
  });
};
