'use strict';

var socketio = require('socket.io');
var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

app.start = function() {
  // start the web server
  var server = app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
  app.io = socketio.listen(server);

  app.io.on('connection', function(socket) {
    // manage web socket events
    socket.on('pulse', function(msg) {
      console.log(msg);
      app.models.Event.create({name: 'pulse', createdAt: Date.now(), data: msg});
      socket.broadcast.emit('pulse', msg);
    });
  });

  return server;
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err)
    throw err;
  // start the server if `$ node server.js`
  if (require.main === module) {
    app.start();
  }
});
