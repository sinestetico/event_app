const BUFFERING_TIME_MILLIS = 15000;
const UP_TO_SPEED_TIME_MILLIS = 5000;

function User(pos, cPoint_a, cPoint_b)
{
    this.id = User.count++;
    this.position = pos;
    this.splines = {};
    this.hr = -1;
    this.hrBuffer = [];
    this.cPoint_a = cPoint_a;
    this.cPoint_b = cPoint_b;

    this.setSplineForNeighbor = function(neighborId)
    {
        var curve = [this.position, center, users[neighborId].position];
        var points = [];

        var distance = 1;
        for(;;)
        {
            var p = jsBezier.pointAlongCurveFrom(curve, 1, -distance);
            p.distance = PIXI.Point.distance;
            if(p.distance(center) > RADIUS)
                break;
            points.push(p);
            distance++;
        }
        this.splines[neighborId] = points;
    };

    this.setSplineForSelf = function()
    {
        const OFFSET_X = RADIUS / 8;
        var controlPoint_a = new PIXI.Point(center.x - OFFSET_X, center.y - 100);
        var controlPoint_b = new PIXI.Point(center.x + OFFSET_X, center.y - 100);
        var start = new PIXI.Point(RADIUS, 2 * RADIUS)
        var curve = [start, controlPoint_a, controlPoint_b, start ];
        var points = [];

        var distance = 1;
        for(;;)
        {
            var p = jsBezier.pointAlongCurveFrom(curve, 1, -distance);
            //p.distance = PIXI.Point.distance;
            if(Math.pow(p.x - center.x, 2) + Math.pow(p.y - center.y, 2) > Math.pow(RADIUS, 2))
                break;
            points.push(p);
            distance++;
        }
        this.foreverAloneSpline = points;
    };


    var that = this;

    function initHR()
    {
        setTimeout(function()
        {
            setHR();
            setInterval(setHR, UP_TO_SPEED_TIME_MILLIS);
        }, BUFFERING_TIME_MILLIS);
    }

    function setHR()
    {
        var nextHR = that.hrBuffer.length * 4;

        if(!nextHR)
            return;
        nextHR -= nextHR % BEATS_RANGE;

        if(that.hr !== nextHR)
        {
            if(that.hr in connections)
            {
                connections[that.hr].removeUser(that.id);
                console.log(that.hr + " <-- " + connections[that.hr].getUsers() + " (removed " + that.id + ")");
            }
            if(nextHR in connections)
            {
                connections[nextHR].addUser(that.id);
                console.log(nextHR + " --> " + connections[nextHR].getUsers());
            }
        }
        that.hr = nextHR;
    }

    this.pulse = function()
    {
        var timestamp = Date.now();

        while(this.hrBuffer.length && timestamp - this.hrBuffer[0] >= BUFFERING_TIME_MILLIS)
            this.hrBuffer.shift();

        this.hrBuffer.push(timestamp);
    };

    initHR();
}

User.count = 0;