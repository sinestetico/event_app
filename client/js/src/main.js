$(function()
{
    app = new PIXI.Application(window.innerWidth, window.innerHeight, { backgroundColor : 0x000000, antialias : true });
    $("body").append(app.view);

    center = new PIXI.Point(RADIUS, RADIUS);

    display = new PIXI.DisplayObjectContainer();

    //display.x = window.innerWidth / 2 - RADIUS;
    display.x = 429.5;
    display.y = 36;
    display.pivot.set(0.5, 0.5);
    //display.position.set(window.innerWidth / 2 - RADIUS, window.innerHeight / 2 - RADIUS);

    app.stage.addChild(display);
    graphics = new PIXI.Graphics();
    //display.addChild(graphics);
    //var arc  = Math.PI * 2 / SECTORS;

    PIXI.Point.distance = function(p)
    {
        return Math.sqrt(Math.pow(this.x - p.x, 2) + Math.pow(this.y - p.y, 2));
    };

    for(var i = 0; i <= 180; i+= BEATS_RANGE)
    {
        connections[i] = new Connection(i);
    }

    initUsers();
    initSplines();
    initSockets();
    /*$.getJSON("res/userSplines.json", function(data)
    {
        for(var i in data)
        {
            users[i].splines = data[i];
        }

        initSockets();
    });*/

    setInterval(function()
    {
        update = true;
    }, UPDATE_INTERVAL);

    // Listen for animate update
    app.ticker.add(function (delta)
    {
        if(update)
        {
            update = false;
            //graphics.clear();
            for(var i in connections)
            {
                var c = connections[i];
                renderer.drawConnections(c);
            }
        }
    });
});

function runTest()
{
    var test60 = [10, 20, 29];
    var test120 = [1, 2, 3];
    var test90 = [4, 5, 6];
    var test180 = [7, 11, 18, 21, 28];
    var test100 = [23];

    /*setInterval(function()
    {
        for(var i in users)
            socketHR.emit('pulse', {deviceId: i});
    }, 60000 / 120);


    return;*/

    if(CLIENT_ONLY)
    {
        setInterval(function()
        {
            for(var i = 0; i < test120.length; i++)
                $(document).trigger('pulse', {deviceId: test120[i]});
        }, 60000 / 120);

        setInterval(function()
        {
            for(var i = 0; i < test90.length; i++)
                $(document).trigger('pulse', {deviceId: test90[i]});
        }, 60000 / 90);

        setInterval(function()
        {
            for(var i = 0; i < test60.length; i++)
                $(document).trigger('pulse', {deviceId: test60[i]});
        }, 60000 / 60);

        setInterval(function()
        {
            for(var i = 0; i < test180.length; i++)
                $(document).trigger('pulse', {deviceId: test180[i]});
        }, 60000 / 180);


        setInterval(function()
        {
            for(var i = 0; i < test100.length; i++)
                $(document).trigger('pulse', {deviceId: test100[i]});
        }, 60000 / 100);
    }

    else
    {
        setInterval(function()
        {
            for(var i = 0; i < test120.length; i++)
                socketHR.emit('pulse', {deviceId: test120[i]});
        }, 60000 / 120);

        setInterval(function()
        {
            for(var i = 0; i < test90.length; i++)
                socketHR.emit('pulse', {deviceId: test90[i]});
        }, 60000 / 90);

        setInterval(function()
        {
            for(var i = 0; i < test60.length; i++)
                socketHR.emit('pulse', {deviceId: test60[i]});
        }, 60000 / 60);

        setInterval(function()
        {
            for(var i = 0; i < test180.length; i++)
                socketHR.emit('pulse', {deviceId: test180[i]});
        }, 60000 / 180);


        setInterval(function()
        {
            for(var i = 0; i < test100.length; i++)
                socketHR.emit('pulse', {deviceId: test100[i]});
        }, 60000 / 100);
    }
}

function initSockets()
{
    //socketHR = io('http://156.148.132.237:4001/');
    /*socketHR = io('http://localhost:4001/');*/

    if(CLIENT_ONLY)
    {
        $(document).on("pulse", function(pulse, data)
        {
            users[data.deviceId].pulse();
        });
    }
    else
    {
        socketHR = io();
        socketHR.on("pulse", function(pulse)
        {
            users[pulse.deviceId].pulse();
        });
    }

    runTest();
}

function initUsers()
{
    graphics.lineStyle(5, 0xff0000, 1);
    graphics.drawCircle(center.x, center.y, RADIUS); // cx, cy, radius, startAngle, endAngle

    var angles = [];
    for(var i = START_IDX; i <= START_IDX + SECTORS; i++)
    {
        var a = i / SECTORS * Math.PI * 2;
        var x = center.x + RADIUS * Math.cos(a);
        var y = center.y + RADIUS * Math.sin(a);

        //const CONTROL_POINT_OFFSET = 10;
        //var cPoint_a = new PIXI.Point(x - CONTROL_POINT_OFFSET - RADIUS * Math.cos(a), center.y);
        //var cPoint_b = new PIXI.Point(x + CONTROL_POINT_OFFSET - RADIUS * Math.cos(a), center.y);
        //var user = new User(new PIXI.Point(x, y), cPoint_a, cPoint_b);

        var user = new User(new PIXI.Point(x, y));
        angles.push(a);
        points.push(user.position);
        users[user.id] = user;
    }

    /*var SUBSECTORS = 11;
    for(var i = 0; i < angles.length - 1; i++)
    {

        for(var j = 0; j <= SUBSECTORS; j++)
        {
            var a_i = j / SUBSECTORS * (angles[i + 1] - angles[i]);
            a_i += angles[i];
            var x = center.x + RADIUS * Math.cos(a_i);
            var y = center.y + RADIUS * Math.sin(a_i);

            graphics.lineStyle(1, 0xffffff, 1);
            graphics.moveTo(x, y);
            graphics.lineTo(center.x, center.y);
        }
    }*/

    for(var i = 0; i < SECTORS; i++)
    {
        var a = i / SECTORS * Math.PI * 2;
        var x = center.x + RADIUS * Math.cos(a);
        var y = center.y + RADIUS * Math.sin(a);

        graphics.lineStyle(5, 0xffff00, 1);
        graphics.moveTo(x, y);
        graphics.lineTo(center.x, center.y);
    }
}

function initSplines()
{
    for(var i in users)
    {
        var user = users[i];

        for(var j in users)
        {
            if(j !== i)
                user.setSplineForNeighbor(j);
            else
            {
                user.setSplineForSelf();
            }
        }
    }
}

function saveJSON(filename, data)
{
    var blob = new Blob([data], {type: 'application/json'});
    var elem = window.document.createElement('a');
    elem.href = window.URL.createObjectURL(blob);
    elem.download = filename;
    document.body.appendChild(elem);
    elem.click();
    document.body.removeChild(elem);
}
