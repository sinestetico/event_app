var renderer = {};
var splines = {};
renderer.drawConnections = function(connection)
{
    if(!connection.connected_users.length)
        return;
    if(!(connection.spline))
    {
        var pts = [];

        if(connection.connected_users.length > 1)
        {
            for(var i = 0; i < connection.connected_users.length - 1; i++)
            {
                var user = users[connection.connected_users[i]];
                pts = pts.concat(user.splines[connection.connected_users[i + 1]]);
            }

            var user = users[connection.connected_users[connection.connected_users.length - 1]];
            pts = pts.concat(user.splines[connection.connected_users[0]]);

            //if(connection.rotated)
            //{
                //console.log("re-rotating " + connection.hr);
                for(var j = 0; j < connection.graphicsBuffer.length; j++)
                    connection.graphicsBuffer[j].rotation = connection.graphicsBuffer[j].initialRotation;

                //connection.rotated = false;
            //}
        }
        else
        {
            var user = users[connection.connected_users[0]];
            pts = pts.concat(user.foreverAloneSpline);

            //if(!connection.rotated)
            //{
                for(var j = 0; j < connection.graphicsBuffer.length; j++)
                    connection.graphicsBuffer[j].rotation = 3 * (Math.PI / 2) + (user.id + START_IDX) * (2 * Math.PI / SECTORS) + connection.graphicsBuffer[j].initialRotation;

                //connection.rotated = true;
            //
            // }

            /*if(!user.rotated)
            {
                user.rotated = true;
                for(var j = 0; j < connection.graphicsBuffer.length; j++)
                    connection.graphicsBuffer[j].rotation = 3 * (Math.PI / 2) + (2 * user.id * Math.PI / SECTORS);
            }*/
        }

        //splines[splineKey] = new Spline(splineKey, pts, connection.hr);
        var spline = new Spline("", pts, connection.hr);
        connection.setSpline(spline);
    }

    renderer.drawSpline(connection, connection.hr);
};


renderer.drawSpline = function(connection, bpm)
{
    var g = connection.graphicsBuffer[connection.getLastDrawnGraphicsIdx()];
    if(g.complete)
    {
        g.clear();
        g.alpha = 1;
        g.complete = false;
    }
    g.lineStyle(5, connection.color, 1);
    //g.lineStyle(2, 0xff0000, 1);
    for(var i = connection.spline.currentIdx; i < connection.spline.currentIdx + connection.spline.step && i < connection.spline.points.length - 1; i++)
    {
        var p1 = connection.spline.points[i];
        var p2 = connection.spline.points[i + 1];
        g.moveTo(p1.x, p1.y);
        g.lineTo(p2.x, p2.y);
    }

    if(connection.spline.currentIdx >= connection.spline.points.length)
    {
        g.complete = true;
        connection.setGraphicsIdx();
        connection.spline.currentIdx = 0;
        connection.fadeOutGraphics(g, bpm);

        //if(!connection.blinking)
        //{
        //    connection.blink(bpm);
        //}
    }
    else
        connection.spline.currentIdx += connection.spline.step;
};
