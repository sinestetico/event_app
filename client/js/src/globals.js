var socketHR;

var app;
var graphics;
const SECTORS = 30;
//const RADIUS = Math.min(window.innerWidth, window.innerHeight) / 2;
const RADIUS = 320;
var points = [];
var center;
var users = {};
const START_IDX = parseInt(SECTORS / 2);

var connections = {};
var url = new URL(window.location.href);
const BEATS_RANGE = parseInt(url.searchParams.get("range")) || 10;
const CLIENT_ONLY = url.searchParams.get("clientOnly") === "true";

var display;
const FPS = 60;
const UPDATE_INTERVAL = 1000 / FPS;
var update = false;

var rotateView = function(deg)
{
    var rot = deg * Math.PI / 180;

    for(var i in connections)
    {
        var conn = connections[i];

        for(var j = 0; j < conn.graphicsBuffer.length; j++)
        {
            conn.graphicsBuffer[j].rotation += rot;
        }
    }
};
