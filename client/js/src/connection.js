function Connection(hr)
{
    const TOTAL_GRAPHICS = 10;

    this.hr = hr;
    this.spline = null;
    this.graphicsBuffer = [];
    var connected_users = [];
    this.connected_users = connected_users;
    this.blinking = false;
    this.color = null;
    this.blinkInterval = null;
    var lastDrawnGraphicsIdx = 0;

    this.addUser = function(id)
    {
        if(!this.color)
        {
            const ROTATION_STEP = 0.0189;
            //const ROTATION_STEP = 0;
            var curr_rotation = -ROTATION_STEP * TOTAL_GRAPHICS;

            console.log(this.color);
            this.color = Connection.palette[Connection.currColorIdx];

            for(var i = 0; i < TOTAL_GRAPHICS; i++)
            {
                var g = new PIXI.Graphics();
                g.lineStyle(5, this.color, 1);
                g.pivot.set(RADIUS, RADIUS);
                g.position.set(RADIUS, RADIUS);


                g.rotation = curr_rotation;
                g.initialRotation = parseFloat(curr_rotation + "");
                curr_rotation += ROTATION_STEP;

                this.graphicsBuffer.push(g);
                display.addChild(g);
            }
            Connection.currColorIdx = (Connection.currColorIdx + 1) % Connection.palette.length;
        }
        id = parseInt(id);

        var currLength = connected_users.length;

        if(!connected_users.length)
            connected_users.push(id);
        else if(connected_users.indexOf(id) === -1)
        {
            for(var i = 0; i < connected_users.length; i++)
            {
                if(connected_users[i] > id) // Sort automatically
                    break;
            }

            connected_users.splice(i, 0, id);
        }

        if(connected_users.length > currLength && this.spline !== null)
        {
            console.log("clearing graphics for " + this.hr);
            for(var i = 0; i < this.graphicsBuffer.length; i++)
                this.graphicsBuffer[i].clear();
            this.spline = null;
        }
    };

    this.removeUser = function(id)
    {
        id = parseInt(id);
        var currLength = connected_users.length;

        var idx = connected_users.indexOf(id);
        if(idx !== -1)
            connected_users.splice(idx, 1);

        if(connected_users.length === 0 || connected_users.length < currLength && this.spline !== null)
        {
            console.log("clearing graphics for " + this.hr);
            //this.stopBlinking();
            //this.graphics.clear();
            for(var i = 0; i < this.graphicsBuffer.length; i++)
                this.graphicsBuffer[i].clear();
            this.spline = null;
        }
    };

    this.getUsers = function()
    {
        return connected_users.join();
    };

    this.setSpline = function(spline)
    {
        this.spline = spline;
    };

    this.blink = function(bpm)
    {
        this.blinking = true;
        this.fadeIn(bpm);
    };

    this.fadeOut = function(bpm)
    {
        var that = this;

        var fdOutInt = setInterval(function()
        {
            for(var i = 0; i < that.graphicsBuffer.length; i++)
                that.graphicsBuffer[i].alpha -= 0.1;

            if(that.graphicsBuffer[0].alpha <= 0)
            {
                clearInterval(fdOutInt);
                that.fadeIn(bpm);
            }
        }, 1);

    };

    this.fadeIn = function(bpm)
    {
        for(var i = 0; i < this.graphicsBuffer.length; i++)
            this.graphicsBuffer[i].alpha = 0;

        var that = this;

        var fdInInt = setInterval(function()
        {
            for(var i = 0; i < that.graphicsBuffer.length; i++)
                that.graphicsBuffer[i].alpha += 0.1;

            if(that.graphicsBuffer[0].alpha >= 1)
            {
                clearInterval(fdInInt);
                setTimeout(function() { that.fadeOut(bpm); }, 60000 / bpm / 20);
            }
        }, 60000 / bpm / 10);
    };

    this.stopBlinking = function()
    {
        clearInterval(this.blinkInterval);
        this.blinking = false;
        this.graphics.alpha = 1;
    };

    this.getLastDrawnGraphicsIdx = function()
    {
        return lastDrawnGraphicsIdx;
    };

    var direction = 1;
    this.setGraphicsIdx = function()
    {
        if(lastDrawnGraphicsIdx === 0 && direction === -1)
            direction *= -1;
        else if(lastDrawnGraphicsIdx === this.graphicsBuffer.length - 1 && direction === 1)
            direction *= -1;

        lastDrawnGraphicsIdx += direction;
    };

    this.fadeOutGraphics = function(g, bpm)
    {
        var fdOutInt = setInterval(function()
        {
            g.alpha -= 0.05;

            if(g.alpha <= 0.3)
            {
                clearInterval(fdOutInt);
            }
        }, (60000 / bpm / 10) * 2);
    };
}

Connection.palette =
    [
        0XE3CC29,
        0XF2A142,
        0XEA6241,
        0XA5459C,
        0X2170B4,
        0X5CA14B,
        0X91604D,
        0XECB936,
        0XF5863B,
        0XDC4E55,
        0XD44786,
        0X4563A5,
        0X009A6D,
        0XBA664B
    ];

Connection.currColorIdx = 0;

